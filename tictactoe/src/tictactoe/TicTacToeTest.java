package tictactoe;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * This tests the TicTacToe class.
 *
 * @author Mark Utting
 */
public class TicTacToeTest {

	protected TicTacToe game;

	@Before
	public void setUp() {
		game = new TicTacToe();
	}

	@Test
	public void testEmpty() {
		// check that the four corners are empty
		assertFalse(game.isTaken(0, 0));
		assertFalse(game.isTaken(0, 2));
		assertFalse(game.isTaken(2, 0));
		assertFalse(game.isTaken(2, 2));
		assertEquals(0, game.winner());
	}

	@Test
	public void testFirstMove() {
		assertFalse(game.isTaken(1, 1));
		game.move1(1, 1);
		assertTrue(game.isTaken(1, 1));
	}

	/** A correct game where player 1 wins along a row. */
	@Test
	public void testWinner1Row() {
		assertEquals(0, game.winner());
		game.move1(0,0);
		game.move2(1,0);
		game.move1(0,1);
		game.move2(1,1);
		assertEquals(0, game.winner());
		game.move1(0,2);
		assertEquals(1, game.winner());
	}

	/** A correct game where player 2 wins down a column. */
	@Test
	public void testWinner2Col() {
		assertTrue(game.move2(2,1));
		assertTrue(game.move1(1,2));
		assertTrue(game.move2(0,1));
		assertTrue(game.move1(0,2));
		assertEquals(0, game.winner());
		assertTrue(game.move2(1,1));
		assertEquals(2, game.winner());
	}

	/** A correct game where player 1 wins along a diagonal. */
	@Test
	public void testWinner1Diag() {
		assertTrue(game.move1(0,2));
		assertTrue(game.move2(0,0));
		assertTrue(game.move1(1,1));
		assertTrue(game.move2(1,0));
		assertEquals(0, game.winner());
		assertTrue(game.move1(2,0));
		assertEquals(1, game.winner());
	}
	
	/** A correct game where a draw occurs. */
	@Test
	public void testWinnerDraw() {
		assertEquals(0, game.winner());
		game.move1(0,0);
		game.move2(0,1);
		game.move1(1,1);
		game.move2(0,2);
		game.move1(1,2);
		game.move2(1,0);
		game.move1(2,0);
		game.move2(2,2);
		assertEquals(0, game.winner());
		game.move1(2,1);
		assertEquals(3, game.winner());
	}
	
	@Test
	public void testGetCell() {
		assertEquals(0, game.getCell(0, 0));
		assertEquals(0, game.getCell(1, 1));
		game.move2(0, 0);
		assertEquals(2, game.getCell(0, 0));
		game.move1(1, 1);
		assertEquals(1, game.getCell(1, 1));
	}

	@Test
	public void testDisplay() {
		game.move2(0, 0);
		game.move1(1, 1);
		game.move2(2, 0);
		game.move1(2, 2);
		// check the display of this game
		String[] strs = game.display();
		assertEquals(3, strs.length);
		assertEquals(" 2 . .", strs[0]);
		assertEquals(" . 1 .", strs[1]);
		assertEquals(" 2 . 1", strs[2]);
	}

	@Test
	public void testInvalidCells() {
		assertTrue(game.move2(0,2));
		assertFalse(game.move1(0,2)); // cell taken
		assertTrue(game.move1(0,1));
	}

	@Test
	public void testInvalidTurn1() {
		assertTrue(game.move1(0,0)); 
		assertFalse(game.move1(1,1)); // not his/her turn
		assertTrue(game.move2(1,1)); // but player 2 can move there
	}
	
	@Test
	public void testInvalidTurn2() {
		assertTrue(game.move2(0,0)); 
		assertTrue(game.move1(1,1));
		assertFalse(game.move1(2, 2)); // not his/her turn
		assertTrue(game.move2(2, 0));  // but player 2 can move
		assertFalse(game.move2(2, 2)); // another wrong turn
	}

	@Test
	public void testInvalidMoveGameOver() {
		assertTrue(game.move1(0, 2));
		assertTrue(game.move2(1, 1));
		assertTrue(game.move1(1, 2));
		assertTrue(game.move2(0, 1));
		assertTrue(game.move1(2, 2)); // player 1 wins
		assertFalse(game.move2(2, 1)); // game already won
	}

}
