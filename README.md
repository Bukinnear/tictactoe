# README #

This README documents how to use this simple Tic Tac Toe game.
It is for ICT221 students at the University of the Sunshine Coast, Australia.

### What is this repository for? ###

* Quick summary: A Tic Tac Toe game for ICT221 students.
* Version: 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo) to see how this README file is created.

### How do I get set up? ###

* Summary of set up: clone this repository using Eclipse. (File / Import / Git / Projects from Git / Clone URI, then enter the URI of this page and follow instructions)
* How to run the game: Run tictactoe/src/tictactoe/TextUI.java as a Java application.
* How to run tests: Run tictactoe/src/tictactoe/TicTacToeTest.java as JUnit Tests.
* Dependencies: none.  (Just Java 8).

### Who do I talk to? ###

* Repo owner: [Mark Utting](mailto:utting@usc.edu.au)